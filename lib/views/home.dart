import 'package:curso_flutter/views/description.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}


class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white, //Paso 2: Cambiamos el fondo del Appbar
        elevation: 10, //Paso 3: Se agrega un 10 de elevacion
        title: Text('Mis Cartas', style: TextStyle(color: Colors.black87),),//Paso 1: Cambiamos el texto y el color del titulo
      ),
      body: ListView(
        //scrollDirection: Axis.horizontal,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.50,
                height: 300,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                          child: Image.network("https://ms.yugipedia.com//1/17/DarkMagicianofChaos-SR08-EN-C-1E.png")),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.50,
                height: 300,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0,10.0,10,0),
                      child: Container(
                          child: Text("Imagen desde el navegador")
                      ),
                    ),
                    RaisedButton(//Paso 4: Se agrega un boton
                      onPressed: () {//Paso 5: Navegaremos a la pantalla descripcion al momento de presionar el boton ver mas
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Description(title: "Imagen desde el navegador",)), //Paso 7: Se agrega el titulo de la pantalla
                        );
                      },
                      child: Text("Ver Más"), //Paso 6: Agregamos el texto para el boton
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.50,
                height: 300,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                          child: Image.asset("assets/img/img-container.webp")
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.50,
                height: 300,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0,10.0,10,0),
                      child: Container(
                          child: Text("Imagen desde los assets"),
                      ),
                    ),
                    RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Description(title: "Imagen desde los assets",)),
                        );
                      },
                      child: Text("Ver Más"),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}