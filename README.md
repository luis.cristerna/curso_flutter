<br>

<center><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Google-flutter-logo.png/799px-Google-flutter-logo.png" height="100"></center>

# curso_flutter

Este proyecto contendrá el código visto durante el curso de Flutter impartido por el desarrollador de software Luis Cristerna.

# Tabla de contenido
1. [Presentación](#Presentación)
2. [Introducción](#Introducción)
   1. [¿Qué es Flutter?](#Flutter)
   2. [¿Cómo funciona?](#Flutter)
   3. [Ventajas](#Ventajas)
   4. [Desventajas](#Desventajas)
   5. [Alternativas](#Alternativas)
3. [Instalación](#Instalación)
4. [Estructura de la aplicación](#Estructura de la aplicación)
5. [Estructura del proyecto y paquetes](#Estructura del proyecto y paquetes)
   1. [Cambiar icono de la aplicación](#Cambiar icono de la aplicación)
6. [Assets, Imágenes, Columnas, Filas y Listview](#Flutter)
7. [Navegación y enviar información entre pantallas](#Navegación y enviar información entre pantallas)

# Presentación

<center>
<img src="https://media-exp1.licdn.com/dms/image/C4E03AQFcnECRkOmtdQ/profile-displayphoto-shrink_800_800/0/1534369674964?e=1658361600&v=beta&t=oG2jeL4Lm6gdYDOLDFdRXh83IHcTgMkfkJpZj_z-wTo" height="300">
<img src="https://meliorcode.com/assets/global/img/logo-melior-code-vertical-fondo-blanco.png" height="300">
</center>

### **Luis Manuel Cristerna Gallegos**
### **Cofundador e Ingeniero de Software en Melior Code**
### **4 años y medio de experiencia como desarrollador de software y 3 como desarrollador móvil**
### **9 Aplicaciones en Android e IOS publicadas en tiendas (6 Siguen publicadas)**

# Introducción

## ¿Qué es Flutter? 

Es un SDK desarrollado por google y en su inicio tenía como objetivo crear aplicaciones móviles tanto para Android como para iOS/iPadOS, aunque desde la última versión estable ya es posible realizar aplicaciones web y de escritorio para macOs, windows y linux.

**Dato curioso:** Al principio era un proyecto interno de google y al ver su potencial decidieron publicarlo como código libre.

<br>

## ¿Cómo funciona?

Flutter utiliza un motor gráfico, llamado **Skia**, que renderiza en 2D los elementos gráficos, dichos elementos se les llama **Widgets** y se puede tratar desde un texto o un botón hasta widgets más complejos que se forman a partir de otros widgets.

<br>

## Ventajas

**Desarrollo Rápido**

Trae tu aplicación a la vida en cuestión de milisegundos con Hot Reload. Utilice un completo conjunto de widgets totalmente personalizables para crear interfaces nativas en cuestión de minutos.

**UI Expresiva y Flexible**

Monta rápidamente funcionalidades con el foco en la experiencia de usuario nativa. La arquitectura en capas permite una completa personalización, que resultan en un renderizado increíblemente rápido y diseños expresivos y flexibles.

**Rendimiento Nativo**

Los widgets de Flutter incorporan todas las diferencias críticas entre plataformas, como el scrolling, navegación, iconos y fuentes para proporcionar un rendimiento totalmente nativo tanto en iOS como en Android.

<br>

## Desventajas

**Competencia**

Existen varias tecnologías similares a flutter y en el futuro pueden existir aún más.

**Evolución**

Primero se desarrolla para android (Google) y para IOS (Apple) y después se desarrolla los módulos para Flutter (Google) y lo mismo pasa al momento de corregir los errores.

**Peso**

Una aplicación multiplataforma pesa más que una aplicación nativa.

<br>

## Alternativas

**React Native**

Flutter es comparado con React Native, porque ambas plataformas usan la programación reactiva y sean plataformas parecidas, cada una tiene su enfoque y utilidad para crear Apps.

En estos momentos Google está corrigiendo bugs e incluyendo nuevas funciones a Flutter bastante rápido, mientras que el progreso de React Native está un poco estancado. Hace poco que se lanzó la versión 0.6 con algunas mejoras.

**WebApps (Cordoba)**

Tienen un enfoque totalmente distinto. Las WebApps están escritas en código HTML, CSS y JavaScript, y realmente la aplicación emula a un navegador web donde se ejecuta el código.

**Xamarin**

Xamarin es la alternativa a tener en cuenta si se quiere compilar en nativo. Fue comprada por Microsoft y permite crear aplicaciones para Android, iOS y Windows Phone, aunque Microsoft ya ha comunicado que dejará el desarrollo de este sistema operativo móvil. 

<br>

# Instalación

A continuación dejo el link para el manual de instalación en flutter y para la versión de flutter que vamos a utilizar, en este caso será la versión 2.2.3

- [ ] [Manual de instalación de flutter](https://esflutter.dev/docs/get-started/install)
- [ ] [Flutter 2.2.3 - Windows](https://storage.googleapis.com/flutter_infra_release/releases/stable/windows/flutter_windows_2.2.3-stable.zip)
- [ ] [Flutter 2.2.3 - macOS](https://storage.googleapis.com/flutter_infra_release/releases/stable/macos/flutter_macos_2.2.3-stable.zip)
- [ ] [Flutter 2.2.3 - Linux](https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_2.2.3-stable.tar.xz)

<br>

# Estructura de la aplicación

<center><img src="https://docs.flutter.dev/assets/images/docs/get-started/ios/starter-app.png"></center>

<br>

# Estructura del proyecto y paquetes

## Cambiar icono de la aplicación

La ruta para agregar **paquetes** es la siguiente

```
pubspec.yaml
```

Agregaremos el directorio llamado **assets/img** en la raíz del proyecto


Link del paquete **flutter_launcher_icons**

- [ ] [Cambiar Iconos](https://pub.dev/packages/flutter_launcher_icons)


Ruta de los iconos en android

```
android/app/src/main/res/mipmap-hdpi/ic_launcher.png
android/app/src/main/res/mipmap-mdpi/ic_launcher.png
android/app/src/main/res/mipmap-xhdpi/ic_launcher.png
android/app/src/main/res/mipmap-xxhdpi/ic_launcher.png
android/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png
```

Ruta de los iconos en IOS

```
ios/Runner/Assets.xcassets/AppIcon.appiconset/
```

Comandos para obtener los paquetes

```
flutter pub get
```

Comandos para ejecutar la configuracion del paquete **flutter_launcher_icons**

```
flutter pub get
flutter pub run flutter_launcher_icons:main
```

<br>

# Assets, Imágenes, Listview, Columnas y Filas

La ruta para agregar **assets** es la siguiente

```
pubspec.yaml
```

# Navegación y enviar información entre pantallas










