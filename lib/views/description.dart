import 'package:flutter/material.dart';// Paso 1: Importamos la clase material


class Description extends StatelessWidget {//Paso 2: Creamos la clase Description que es una StatelessWidget (Explicar)

  final String title; //Se agrega el titulo del tipo string

  const Description({Key? key, required this.title}) : super(key: key); ///Se agrega el atributo como argumento en el constructor

  @override
  Widget build(BuildContext context) {
    return Scaffold(//Paso 3: Agregamos un Scaffold
      appBar: AppBar(//Paso 4: Agregamos un appbar
        title: Text(title), //Paso 5: Agregamos titulo a el appbar
        automaticallyImplyLeading: false, //Paso 9: Quitamos el leading por default
      ),
      body: Center( //Paso 6: Agregamoms un body con un center
        child: RaisedButton(//Paso 7: Agregamos un boton
            onPressed: () {//Paso 9: Eliminamos la Route actual de la pila de rutas administradas por Navigator
              Navigator.pop(context);
            },
            child: Text('Regresar'), //Paso 8: Agregamos el texto para el boton
        ),
      ),
    );
  }
}
