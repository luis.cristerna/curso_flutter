import 'package:flutter/material.dart';
import 'package:curso_flutter/views/home.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Curso Flutter',
      home: Home(),
    );
  }
}


